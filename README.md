# Jenkins Master-Slave(s) cluster as a Docker service.

Tested on Debian9, docker-compose 1.23.2 .

## [Install Docker](https://docs.docker.com/install/linux/docker-ce/debian/)

1. Enable apt to use a repository over HTTPS:
```
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common
```

2. Add Docker’s official GPG key:
```
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
```

3. Set up the stable repository:
```
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
```

4. Update and install:
```
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io
```

## [Install Docker Compose](https://docs.docker.com/compose/install/)


1. Download
```
curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

2. Apply executable permissions
```
chmod +x /usr/local/bin/docker-compose
```

## Run the service

Run `docker-compose up`
Remark: consider using `docker secret` to encrypt the private key in `ssh-keys/id-rsa` (right now it is a through_away demo key).

Then browse to `localhost:8888` and supply Jenkins' initialAdminPassword (see output of the container
or file `/var/jenkins_home/secrets/initialAdminPassword` inside the jenkins-master) and install recomended plugins.

Check IPs of the slaves with e.g. `docker inspect jenkins-slave_1`
Enter the master `docker exec -it jenkins-master /bin/bash` and check that you can SSH to both slaves: e.g. `ssh jenkins@172.30.0.3`


## Monitoring

Browse to `localhost:8080` and see cAdvisor in action.
If you need to store the monitoring data persistently - uncomment the Prometheus section in the docker-compose.yml, rerun docker-compose and point your browser on `localhost:9090`.




